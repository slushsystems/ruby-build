FROM slushsystems/ruby:latest

USER root

RUN dnf -y install git ruby-devel redhat-rpm-config \
  && dnf -y groupinstall "Development Tools" \
  && dnf clean all

USER slushy

